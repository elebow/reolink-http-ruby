# frozen_string_literal: true

require "test_helper"

class Reolink::TestHttp < Minitest::Test
  MOCK_HTTP = Class.new do
    attr_reader :request_history

    def initialize
      @request_history = []
    end

    def post(*args)
      @request_history.append([:post, args])

      response_body = if args[0] == "/api.cgi?cmd=Login"
                        [{ cmd: "", code: "", value: { Token: { name: "great-token", leaseTime: 150 } } }]
                      else
                        [{ cmd: "", code: "", value: "ggg" }]
                      end
      OpenStruct.new(body: response_body.to_json)
    end
  end.new

  def setup
    Net::HTTP.define_singleton_method(:start) do |*args, **kwargs, &block|
      block.call(MOCK_HTTP)
    end
  end

  def test_that_it_has_a_version_number
    refute_nil ::Reolink::HTTP::VERSION
  end

  def test_it_builds_requests_correctly
    camera_http = Reolink::HTTP.new("camera-url")
    camera_http.get_foo

    assert_equal 2, MOCK_HTTP.request_history.size
    assert_equal [:post,
                  ["/api.cgi?cmd=Login",
                   "[{\"cmd\":\"Login\",\"action\":1,\"param\":{\"User\":{\"Version\":\"0\",\"userName\":null,\"password\":null}}}]"]],
                 MOCK_HTTP.request_history[0]
    assert_equal [:post,
                  ["/api.cgi?cmd=GetFoo&token=great-token",
                   "[{\"cmd\":\"GetFoo\",\"action\":1,\"param\":{}}]"]],
                 MOCK_HTTP.request_history[1]

    camera_http.set_foo({ a: 5 })
    assert_equal [:post,
                  ["/api.cgi?cmd=SetFoo&token=great-token",
                   "[{\"cmd\":\"SetFoo\",\"action\":1,\"param\":{\"Foo\":{\"a\":5}}}]"]],
                 MOCK_HTTP.request_history[2]
  end
end
