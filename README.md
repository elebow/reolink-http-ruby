# Reolink::Http

This gem provides a Ruby interface to Reolink's device HTTP API.

This was developed and tested for API version 8 (2023-04).

## Installation

Install the gem and add to the application's Gemfile by executing:

    $ bundle add reolink-http

If bundler is not being used to manage dependencies, install the gem by executing:

    $ gem install reolink-http

## Usage

Refer to Reolink's API documentation for your device. Because the supported commands vary between devices and firmware versions, and because there is no way to reliably probe a device for supported commands, this gem does not attempt to enumerate all commands.

Instead, it uses `method_missing` and places responsibility on the programmer to handle errors for unsupported commands.

Suppose a device supports commands `GetFoo` and `SetFoo`.

```ruby
require "reolink/http"

reolink_http = Reolink::HTTP.new("great-camera", username: "admin", password: nil)

puts reolink_http.get_foo

reolink_http.set_foo({ bar: true })
```

An unsupported command will generate an error response. See the API documentation for all error responses. Example:

```ruby
reolink_http.foo
=> [#<Reolink::HTTP::Response:0x00007f791386a518
     @cmd="Unknown",
     @code=1,
     @error={"detail"=>"not support", "rspCode"=>-9},
     @initial=nil,
     @range=nil,
     @value=nil>]
```

Use the `verbose` parameter to control whether a `Get` response includes the initial value and acceptable range. This parameter is ignored for `Set` commands.

```ruby
reolink_http.get_osd({ channel: 0 })
=> [#<Reolink::HTTP::Response:0x00007fdc43b18f38
     @cmd="GetOsd",
     @code=0,
     @error=nil,
     @initial=
      {"bgcolor"=>0,
       "channel"=>0,
       "osdChannel"=>{"enable"=>1, "name"=>"Camera1", "pos"=>"Lower Right"},
       "osdTime"=>{"enable"=>1, "pos"=>"Top Center"},
       "watermark"=>1},
     @range=
      {"bgcolor"=>"boolean",
       "channel"=>0,
       "osdChannel"=>
        {"enable"=>"boolean",
         "name"=>{"maxLen"=>31},
         "pos"=>["Upper Left", "Top Center", "Upper Right", "Lower Left", "Bottom Center", "Lower Right"]},
       "osdTime"=>
        {"enable"=>"boolean",
         "pos"=>["Upper Left", "Top Center", "Upper Right", "Lower Left", "Bottom Center", "Lower Right"]},
       "watermark"=>"boolean"},
     @value=
      {"bgcolor"=>0,
       "channel"=>0,
       "osdChannel"=>{"enable"=>1, "name"=>"Camera1", "pos"=>"Lower Right"},
       "osdTime"=>{"enable"=>1, "pos"=>"Top Center"},
       "watermark"=>0}>]
```

```ruby
reolink_http.get_osd({ channel: 0 }, verbose: false)
=> [#<Reolink::HTTP::Response:0x00007fdc43b16eb8
     @cmd="GetOsd",
     @code=0,
     @error=nil,
     @initial=nil,
     @range=nil,
     @value=
      {"bgcolor"=>0,
       "channel"=>0,
       "osdChannel"=>{"enable"=>1, "name"=>"Camera1", "pos"=>"Lower Right"},
       "osdTime"=>{"enable"=>1, "pos"=>"Top Center"},
       "watermark"=>0}>]
```

Some other examples:

```ruby
require "reolink/http"

reolink_http = Reolink::HTTP.new("great-camera", username: "admin", password: nil)

reolink_http.set_ir_lights({state: "Off"})

ir_light_state = reolink_http.get_ir_lights
puts ir_light_state[0].value["state"]

snap = reolink_http.snap({ channel: 0, rs: SecureRandom.bytes(16) }, method: :get).body
File.write("snap.jpg", snap)
```

## Contributing

Bug reports and pull requests are welcome at <https://codeberg.org/elebow/reolink-http-ruby>.
