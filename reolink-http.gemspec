# frozen_string_literal: true

require_relative "lib/reolink/http/version"

Gem::Specification.new do |spec|
  spec.name = "reolink-http"
  spec.version = Reolink::HTTP::VERSION
  spec.authors = ["Eddie Lebow"]
  spec.email = ["elebow@users.noreply.github.com"]
  spec.license = "AGPL-3.0-or-later"

  spec.summary = "Interface for Reolink's HTTP device API."
  spec.homepage = "https://codeberg.org/elebow/reolink-http-ruby"
  spec.required_ruby_version = ">= 3.1.0"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://codeberg.org/elebow/reolink-http-ruby"
  spec.metadata["changelog_uri"] = "https://codeberg.org/elebow/reolink-http-ruby/src/CHANGELOG.md"
  spec.metadata["rubygems_mfa_required"] = "true"

  spec.files = Dir["lib/**/*"]
  spec.bindir = "exe"
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]
end
