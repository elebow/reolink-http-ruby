# frozen_string_literal: true

require "json"
require "net/http"

require_relative "http/version"

module Reolink
  # Provides an abstraction of the Reolink device API.
  # See the Reolink API documentation for commands, parameters, and response fields.
  class HTTP
    attr_reader :host, :token, :token_expires_at

    def initialize(host, username: nil, password: nil)
      @host = host
      @username = username
      @password = password
      @token = nil
      @token_expires_at = Time.new(0)
    end

    def method_missing(method_name, params = {}, **kwargs)
      # Assume any missing method is a valid Reolink command. We have no good way to validate this: the API changes and
      # will be a maintenance burden, and GetAbility does not return all commands. An invalid command will still cause
      # a proper error response, so this functionality is correct, if not ideal.
      command(command: method_name.to_s.split("_").map(&:capitalize).join,
              params:,
              **kwargs)
    end

    def respond_to_missing?(*_args)
      true
    end

    def command(method: :post, **kwargs)
      Net::HTTP.start(host, use_ssl: true, verify_mode: OpenSSL::SSL::VERIFY_NONE) do |http|
        refresh_token(http:) if token_expires_at <= Time.now
        post(http:, **kwargs)
      end
    end

    def refresh_token(http:)
      login_response = post(http:, command: "Login", params: { "User" => { "Version" => "0",
                                                                           "userName" => @username,
                                                                           "password" => @password } })
                       .first.value["Token"]
      @token = login_response["name"]
      @token_expires_at = Time.now + login_response["leaseTime"]
    end

    private

    # `verbose` is passed as the `action` parameter. Ignored for non-Get* endpoints.
    #   0 -- get current value only
    #   1 -- get initial, range, and current value
    def post(http:, command:, params: {}, verbose: true)
      params = { command.delete_prefix("Set") => params } if command.start_with?("Set")

      http.post("/api.cgi?cmd=#{command}" + ("&token=#{token}" if token).to_s,
                JSON.generate([{ cmd: command, action: verbose ? 1 : 0, param: params }]))
        .then { |http_response| Response.from_json(http_response.body) }
    end

    # Represents a response from the device.
    #   cmd -- name of command
    #   code -- status code. 0 means no errors.
    #   initial -- initial value of attribute, for Get* requests when `verbose` is true
    #   range -- range of possible values for attribute, for Get* requests when `verbose` is true
    #   current -- current value of attribute, for Get* requests
    #   error -- error code and details. nil if no error.
    class Response
      # Parse a JSON string from the device and return *an array* of Response objects.
      def self.from_json(str)
        JSON.parse(str)
            .map do |response|
              new(**response.transform_keys(&:to_sym))
            end
      end

      attr_reader :cmd, :code, :initial, :range, :value, :error

      def initialize(cmd:, code:, initial: nil, range: nil, value: nil, error: nil)
        @cmd = cmd
        @code = code
        @initial = initial&.[](cmd.delete_prefix("Get")) || initial
        @range = range&.[](cmd.delete_prefix("Get")) || range
        @value = value&.[](cmd.delete_prefix("Get")) || value
        @error = error
      end
    end
  end
end
